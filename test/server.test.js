var expect = require('chai').expect;
const io = require('socket.io-client'); /* Load 'socket.io-client' module */
var tests = 1;

describe('----Testing Server in the chat', function() {

    var socket_1, socket_2;
    var nickname_1 = "Tom1";
    var nickname_2 = "Jerry1";
    var options = { 'force new connection': true };
    const password = "123";

    // Runs before each test in this block
    beforeEach(function (done) {
        // Assuming a server is listening for socket connections
        // Connect to the server
        socket_1 = io('http://localhost:3700', options);
        socket_2 = io('http://localhost:3700', options);

        console.log(">> Test #" + (tests++));
        socket_1.on('connect', function() {
            console.log("socket_1 connected...");
            socket_1.emit('packet', {"sender": nickname_1, "action": "join", "password": password});
        });

        socket_2.on('connect', function() {
            console.log("socket_2 connected...");
            socket_2.emit('packet', {"sender": nickname_2, "action": "join", "password": password});
        });

        socket_1.on('disconnect', function() {
            console.log("socket_1 disconnected");
        });

        socket_2.on('disconnect', function() {
            console.log("socket_2 disconnected\n");
        });

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    });

    // Runs after each test in this block
    afterEach(function (done) {

        // Disconnect io clients
        socket_1.disconnect();
        socket_2.disconnect();

        // The done() callback must be called for Mocha to terminate the test
        // and proceed to the next test, otherwise the test keeps running
        // until the timeout reaches.
        done();
    })

    it('Notify that a user joined the chat', function(done) {
        socket_2.on('packet', function(packet_data) {
            expect(packet_data.action).to.equal("join");
            done();
        });
    });

    it('Broadcast a message to others in the chat', function(done) {
        var msg_hello = "hello socket_2";
        socket_1.emit('packet', {"sender": nickname_1, "action": "broadcast",
            "msg": msg_hello});
        socket_2.on('packet', function(packet_data) {
            if ("broadcast" === packet_data.action) {
                expect(packet_data.msg).to.equal(msg_hello);
                done();
            }
        });
    });

    it('List all users in the chat', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "list"});
        socket_1.on('packet', function(packet_data) {
            // https://www.chaijs.com/guide/styles/#expect
            if ("list" === packet_data.action) {
                expect(packet_data.users).to.be.an('array').that.includes(nickname_1);
                expect(packet_data.users).to.be.an('array').that.includes(nickname_2);
                done();
            }
        });
    });

    it('Notify that a user quit the chat', function(done) {
        socket_1.emit('packet', {"sender": nickname_1, "action": "quit"});
        socket_2.on('packet', function(packet_data) {
            // https://www.chaijs.com/guide/styles/#expect
            if ("quit" === packet_data.action) {
                expect(packet_data.sender).to.equal(nickname_1);
                done();
            }
        });
    });


});

describe('----Testing Server in a group', function() {

    let socket_1 = null, socket_2 = null, socket_3 = null;
    const username_1 = "doraemon";
    const username_2 = "pikachu";
    const username_3 = "nemo";
    const group_name = "minf19";
    const password = "123";

    beforeEach(function (done) {
        socket_1 = io("http://localhost:3700");
        socket_2 = io("http://localhost:3700");
        socket_3 = io("http://localhost:3700");

        console.log(`>> Test #${tests++}`);
        socket_1.on("connect", function () {
            console.log("socket_1 connected...");
            socket_1.emit("packet", {
                sender: username_1,
                action: "join",
                password: password,
            });
            socket_1.emit("packet", {
                sender: username_1,
                action: "jgroup",
                group: group_name,
            });
        });

        socket_2.on("connect", function () {
            console.log("socket_2 connected...");
            socket_2.emit("packet", {
                sender: username_2,
                action: "join",
                password: password,
            });
            socket_2.emit("packet", {
                sender: username_2,
                action: "jgroup",
                group: group_name,
            });
        });

        socket_3.on("connect", () => {
            console.log("socket_3 connected");
            socket_3.emit("packet", {
                sender: username_3,
                action: "join",
                password: password,
            });

        });

        socket_1.on("disconnect", function () {
            console.log("socket_1 disconnected");
        });

        socket_2.on("disconnect", function () {
            console.log("socket_2 disconnected");
        });

        socket_3.on("disconnect", function () {
            console.log("socket_3 disconnected");
        });
        done();
    });

    afterEach(function (done) {
        socket_1.disconnect();
        socket_2.disconnect();
        socket_3.disconnect();
        done();
    });

    it("Notify that a user joined a group", function (done) {
        let runTime = 0;
        socket_1.on("packet", function (data) {
            if (data.action === "jgroup" && runTime === 0) {
                expect(data.sender).to.equal(username_2);
                expect(data.group).to.equal(group_name);
                runTime++; // Prevent asynchronously run multiple done() <-- this will cause exception
                done();
            }
        });
    });




});
